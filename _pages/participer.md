---
layout: page
title: Participer
---

<li><b>Vous êtes une plateforme</b> se retrouvant dans les valeurs de notre charte et manifeste ?<br>
Venez participer à notre écosystème.</li><br>

<li><b>Vous êtes un particulier</b> souhaitant contribuer à Plateformes en Communs et à l'ermergence d'une nouvelle génération de plateformes ?<br>
Nous recherchons de nombreuses compétences en droit des sociétés, droit des licences, en financement de l'ESS, en développement logiciel et web, en communication, en animation de communautés...
<br>Toutes les bonnes volontées sont bienvenues :)</li>
<br>

<li><b>Vous êtes une entreprise</b> et vous souhaitez accompagner ou financer une nouvelle génération de plateformes ? Prenez contact.
<br><br>

<center><a href="{{ site.baseurl }}/contact" class="button button--full-height special">Prenez contact</a></center>
