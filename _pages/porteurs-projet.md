---
layout: page
title: Je souhaite créer une nouvelle plateforme
---

Vous partagez les préoccupations d’inclusion et de participation de votre communauté dans votre plateforme ? Vous souhaitez inclure des démarches prenant en compte les enjeux sociaux et sociétaux de votre activité ? Intégrer la production de Communs dans vos activités ?
Vous souhaitez innover et participer à l’émergence d’une nouvelle génération de plateformes ? Inventer de nouvelles formes de gouvernances plus horizontales ?

En rejoignant Plateformes en Communs :
vous accédez à des plateformes et des communautés étant passé par les mêmes étapes de création que vous traversez (innovation dans la rédaction de statuts, recherche de financements, développement logiciels…
vous accéder aux outils communs créés par les plateformes
vous pouvez créer des convergences entre votre plateforme et celles membres de Plateformes en Communs
Vous accédez à un réseau de communication entre plateformes partageant les mêmes valeurs et pratiques

[Rejoignez-nous au sein de Plateformes en Communs]({{ site.baseurl }}{% link _pages/participer.md %})
