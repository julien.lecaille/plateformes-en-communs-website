---
layout: page
title: Merci pour votre message !
---

Merci d'avoir pris contact avec nous, nous revenons vers vous rapidement !


<a href="/" class="button button--full-height special">Retourner à l'accueil</a>
