---
layout: platform
title: Coop Cycle
description : "Livraison à vélo"
image: coopcycle.jpg
---

## Qu'est-ce que CoopCycle ?

CoopCycle est une plateforme de livraison de repas open-source, utilisable commercialement par des livreurs à vélo constitués en coopérative.

Son objectif est de permettre aux livreurs de posséder leur outil de travail de manière partagée, de créer des conditions favorables à l'auto-organisation des conditions de travail et de bénéficier d'une protection sociale mutualisée.

### Quels objectifs pour CoopCycle ?
CoopCycle est pensée comme une solution pour améliorer les conditions de travail et la protection sociale des livreurs de repas à domicile actuellement précarisés et exploités par les plateformes de livraison capitalistes existant actuellement (Deliveroo, Foodora, Uber Eats, etc.). Cette évolution passera par la réappropriation de leur outil de travail (la plateforme), de leurs conditions de travail (démocratie collégiale, 1 personne 1 voix) et par la mutualisation des services comme des risques via la création de coopératives.

### Quelles sont vos innovations en terme de gouvernances pour permettre à toute votre communauté de participer aux décisions ?
CoopCycle est actuellement une association de préfiguration de coopérative. La gouvernance est collective. Quiconque partageant nos valeurs anti-capitalistes, féministes, écologiques et démocratiques et adhérant à notre charte de fonctionnement interne peut rejoindre l'association. Il n'y a pas de "chef" ou de "leader" et nous fonctionnons sur le principe de l'holocratie. Nous insistons sur la participation de chacun à la prise de décision.

### Quelles approches des communs pour CoopCycle ?
Si on en croit Dardot et Laval, nous vivons "la tragédie du non-commun", le commun étant pourtant le "thème central de l'alternative au néolibéralisme" et le "principe effectif des combats et des mouvements qui "ont (...) résisté à la dynamique du capital" (Commun, p.14). Pour tenter de sortir de cette tragédie, pour rejoindre ces combats et mouvements et parce qu'il faut bien commencer quelque part, nous pensons que l'open source est une piste intéressante selon certaines conditions et une chance pour permettre à un maximum de personnes de contribuer et d'utiliser un service.

En ce sens, nous sommes persuadés que c'est parce que la licence garantie une utilisation ouverte qu'un bien commun devient utile et inappropriable par un tiers. Seulement, nous constatons que la plupart du temps, ce travail gratuit ne revient pas aux "commoners" de façon à ce qu'il puisse y avoir un véritable circuit économique propre aux communs.

Nous croyons donc qu'un système de cotisation est à même de répondre aux nécessités du développement et de l'entretien d'un tel logiciel (voir plus bas notre modèle économique). Nous ne croyons pas que le bénévolat puisse seul changer la donne : il faut qu'un commun puisse récupérer une partie la valeur économique qu'il génère.

La loi n'aide en rien en ce sens puisqu'aucune disposition juridique ne détermine ce qu'est un commun. Le gros du chantier pour nous concerne la licence à attribuer au logiciel : elle doit contraindre (1) l’utilisation par une coopérative et (2) garantir le retour d'une part de la valeur produite dans le pot commun par le biais de la cotisation.

Pour en savoir plus sur nos positions, écoutez l'émission de [Radio Parleur sur le thème des Communs à l'occasion de l'ouverture du Bar Commun à Paris](https://www.radioparleur.net/single-post/2017/10/12/Radio-Commune-cest-quoi-les-Communs-Ecoutez-l%C3%A9mission-en-direct)


### Quel est votre modèle économique ?
Le modèle économique des SCIC (Société coopérative d'intérêt collectif) parisienne et nationale que nous projetons de créer est basé sur un financement par la cotisation des membres de la coopérative (et non sur la facturation d'un service aux coopératives de livreurs) et sur une réduction de nombreux coûts par la mutualisation des ressources administratives et matérielles. Notre plan d'affaires détaillé est en cours d'élaboration.

### Que pouvez-vous mettre en commun pour aider d'autres plateformes ?
Notre ambition est d'arriver à fédérer les initiatives partageant nos positions vers un objectif commun : celui de faire vivre une économie des plateformes qui permette une réelle protection sociale, des rémunérations justes et des conditions de travail choisies par les travailleurs.

L'intérêt de fonctionner en réseau de coopératives est de permettre à des personnes qui font des choses différentes de s'associer et de mutualiser leurs ressources afin d'être plus fortes face à la concurrence des grands groupes capitalistes. Mais nous constatons que, pour l'instant, beaucoup d'initiatives restent locales. Entre les tenants du logiciel libre, les militants des communs, le secteurs des coopératives d'emploi ou encore le monde associatif, il y a des passerelles à créer pour faire émerger des solutions d'intérêt commun. Nous aimerions faire émerger des propositions politiques communes pour construire collectivement des formes d'emploi sans subordination et sans recherche de profit.

Pour cela, en plus de créer une plateforme de livraison et d'accompagner la création de coopératives, nous organisons des réflexions collectives avec différents acteurs et spécialistes des questions économiques, juridiques, techniques et théoriques qui nous occupent. L'objectif est de penser les institutions économiques qui permettront de péréniser et de penser une possible hégémonie de ce mode de production alternatif (sans profit ni patrons) que nous défendons. La première a eu lieu en septembre dernier et est à [(re)voir ici](https://www.youtube.com/watch?v=u7RxqaqjM5E&feature=youtu.be).

### Un conseil à une plateforme qui souhaiterait se créer ?
Nous conseillons aux plateformes coopératives en cours de création de définir clairement leur coeur d'activité. La majorité des charges (développement, juridique, voire la marque) sont mutualisables avec d'autres coopératives. De plus les initiatives étant nombreuses, des retours d'expériences ou des aides ponctuelles sont tout à fait possibles entre coopératives.
